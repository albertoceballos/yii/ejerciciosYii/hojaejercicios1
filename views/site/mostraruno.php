<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model'=>$model,
    'attributes'=>[
        
      [
      'label'=>"Título",
      'value'=>$model->texto,
      ],
    ],
]);
